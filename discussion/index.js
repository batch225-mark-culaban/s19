

// conditional statements - allows us to control the flow of our program

//SECTION if, else if , else statement
/*

Syntax:
  if(condition) {
	statement
  };


*/


//IF statement

let numA = 5;

if(numA > 3) {
	 console.log('Hello');
}


let city = "New York";

if (city === "New York"){
	console.log("Welcome to New York City");
}


// else if Clause


/* -executes a statment if prevvvvioeus condition are false



*/
//else if - parang second option if mg fail si "if"
let numB = 1;

if(numA < 3){ //numA = 5
	console.log('hello');

} else if (numB > 0) {
	console.log('world');
}


city = "Tokyo";

if(city === "New York"){
	 console.log("Welcome to new york city");


}else if(city === "Tokyo"){
	console.log("welcome to tokyo, japan!");
}

//else statement - executes a statement if all other conditions are false
// the ELSE statemenet is optional aand can be added to capture any other result to change the flow of a program.


let numC = -5;
let numD = 7;


if(numC > 0){

console.log("Hello");

} else if (numD === 0){

	console.log("world");
} else {
	console.log("again");
}


//if, else if and else statements with function

function determineTyphoonIntensity(windSpeed){

 if (windSpeed < 30){
	return 'Not a typhoon yer.' ;
}
else if (windSpeed <= 61){
	 return 'Tropical depression detected.' ;
}
else if (windSpeed >= 62 && windSpeed <= 88){
	// && means AND operator
	return 'tropical storm detected' ;
}
else if (windSpeed >=89 || windSpeed <= 117) {// || means OR operator

return 'severe tropical storm detected';

	}
}

let message = determineTyphoonIntensity(70);
console.log(message);



if (message == `tropical storm detected`) {
	console.warn(message);
}

//SECTION conditional TERNARY operation

//Single statement execution

//the conditional (ternary) operator takes in three operands:
//1. condition
//2. expression to execute  if the condition is truthy
//3. expression to execute if the condition is falsey

/*
- ternary operand is for shorthand code. Commonly used for single statement execution where the result consist of only one line of code.

 - SYNTAx
 (expression) ? ifTrue :ifFalse;
 - can be used as an alternative to an "if else" statement

 */




let ternaryResult = ( 1 < 18 ) ? true : false
console.log("Result of Ternary Operator; " + ternaryResult);





let t = "yes";
let f = "no" ;

//bali ang true nasa una pirmi t : f ;
let ternaryResult1 = ( 1 > 18 ) ? t : f
console.log("Result of Ternary Operator; " + ternaryResult1);




//MULTIPLE Statement execution


let name;

function isOfLegalAge() {
    name = 'John';
    return 'You are of the legal age limit';
}

function isUnderAge() {
    name = 'Jane';
    return 'You are under the age limit';
}



// the parseInt function converts the input recieve into a number data type
let age = parseInt(prompt("What is your age?"));
console.log(age);
let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in functions: " + legalAge + ', ' + name);


//SECTION SWITCH STATEMENT 

/*
switch statements evaluates an expression and matches teh expression's value to a case clause. the switch will then execute the statement associated with that case, as well as statements in cases that follow the matching case.

can be used as an alternative to an if, "else if and else" statement where the data to be used in the condition is of an expected output
*/

/*
switch (expression) {
	case value:
		 statement;
		 break;
		 default: statement;
}

*/

let day = prompt("What day of the week is it today?").toLowerCase();// will change input recieved from the pormpt into all lowercase letters ensuring a match with the switch caseconditions if the user inputs capitalized or uppercase letters.
console.log(day);

switch (day) {
    case 'monday': 
        console.log("The color of the day is red");
        break;
    case 'tuesday':
        console.log("The color of the day is orange");
        break;
    case 'wednesday':
        console.log("The color of the day is yellow");
        break;
    case 'thursday':
        console.log("The color of the day is green");
        break;
    case 'friday':
        console.log("The color of the day is blue");
        break;
    case 'saturday':
        console.log("The color of the day is indigo");
        break;
    case 'sunday':
        console.log("The color of the day is violet");
        break;
    default:
        console.log("Please input a valid day");
        break;
}